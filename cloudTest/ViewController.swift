//
//  ViewController.swift
//  cloudTest
//
//  Created by Kyryl Nevedrov on 27.09.16.
//  Copyright © 2016 Kyryl Nevedrov. All rights reserved.
//

import UIKit
import CloudKit
import CoreLocation

class ViewController: UIViewController {

    @IBOutlet weak var imageView: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        let artworkRecordID: CKRecordID = CKRecordID(recordName: "one")
        let artworkRecord: CKRecord = CKRecord(recordType: "Artwork", recordID: artworkRecordID)
//        let artworkRecordIDtwo: CKRecordID = CKRecordID(recordName: "two")
//        let artworkRecordtwo: CKRecord = CKRecord(recordType: "Artwork", recordID: artworkRecordIDtwo)
        let ckContainer: CKContainer = CKContainer.default()
        let publicDataBase: CKDatabase = ckContainer.publicCloudDatabase
        artworkRecord["name"] = "Tom" as CKRecordValue?
        
        let artistRecordID: CKRecordID = CKRecordID(recordName: "Tom")
        let predicate = NSPredicate(format: "name = %@", artistRecordID)
   
        let subscription = CKSubscription(recordType: "Artwork", predicate: predicate, options: .firesOnRecordCreation)
        let notificationInfo = CKNotificationInfo()
        notificationInfo.alertLocalizationKey = "New artwork by your artist"
        notificationInfo.shouldBadge = true
        subscription.notificationInfo = notificationInfo
        publicDataBase.save(subscription) { (subscription, error) in
            guard error == nil else {
                print(error?.localizedDescription)
                return
            }
        }
        
        
        let artworksIDs = [artworkRecordID, artworkRecordIDtwo]
        let fetchRecordOperation: CKFetchRecordsOperation = CKFetchRecordsOperation(recordIDs: artworksIDs)
        
        fetchRecordOperation.perRecordCompletionBlock = { (record, recordId, error) in
            guard error == nil else {
                print(error?.localizedDescription)
                return
            }
            print(record)
        }
        
        fetchRecordOperation.fetchRecordsCompletionBlock = { (records, error) in
            guard error == nil else {
                print(error?.localizedDescription)
                return
            }
            print(records)
            
        }
        fetchRecordOperation.database = publicDataBase
        fetchRecordOperation.start()
        
        
      //  To fetch the target of a one-to-one relationship
        publicDataBase.fetch(withRecordID: artworkRecordID) { (record, error) in
            guard error == nil else {
                print(error?.localizedDescription)
                return
            }
            
            let referenseToArtist: CKReference = record!["artist"] as! CKReference
            let artistRecordID = referenseToArtist.recordID
        
            publicDataBase.fetch(withRecordID: artistRecordID) { (record, error) in
                guard error == nil else {
                    print(error?.localizedDescription)
                    return
                }
                print(record)
            }
            
            //Resolve One-To-Many Relationships
             let predicate: NSPredicate = NSPredicate(format: "artist = %@" , artistRecordID)
            let query = CKQuery(recordType: "Artwork", predicate: predicate)
            
            publicDataBase.perform(query, inZoneWith: nil, completionHandler: { (records, error) in
                guard error == nil else {
                    print(error?.localizedDescription)
                    return
                }
                print(records)
            })
            

        }
        
    
        
       
        
        let artistID = CKRecordID(recordName: "Tom")
        let artistReferense = CKReference(recordID: artistID, action: .none)
        artworkRecord["artist"] = artistReferense
        artworkRecordtwo["artist"] = artistReferense
        
        publicDataBase.save(artworkRecord) { (record, error) in
            guard error == nil else {
                 print(error?.localizedDescription)
                return
            }
        }
    
        
      
        
        let geocoder = CLGeocoder()
        geocoder.geocodeAddressString("ukraine") { (clPlacemarks, error) in
            guard error == nil else{
                print(error?.localizedDescription)
                return
            }
            guard clPlacemarks!.count > 0 else {
                return
            }
            
            let placeMark = clPlacemarks![0]
            artworkRecord["locat"] = placeMark.location
            publicDataBase.save(artworkRecord, completionHandler: { (record, error) in
                guard error == nil else {
                    print(error?.localizedDescription)
                    return
                }
            })
        }
        
        
       
        let location = CLLocation(latitude: 49.3, longitude: 31.0)
        let radius: CGFloat = 20 //in metres
        
        let predicate = NSPredicate(format: "distanceToLocation:fromLocation:(locat, %@) < %f", location, radius)
        print(predicate)
        let query = CKQuery(recordType: "LocationLily", predicate: predicate)
        
        publicDataBase.perform(query, inZoneWith: nil) { (records, error) in
            guard error == nil else{
                 print("Hoyer")
                                print(error?.localizedDescription)
                                return
                            }
        }
        
        
        publicDataBase.save(artworkRecord) { (record, error) in
            guard error == nil else {
                print(error?.localizedDescription)
                return
            }
        }
        publicDataBase.fetch(withRecordID: artworkRecordID) { (ckRecord, error) in
            guard error == nil else {
                print(error!.localizedDescription)
                return
            }
            ckRecord!["name"] = "Lily I love" as CKRecordValue?
            publicDataBase.save(ckRecord!, completionHandler: { (record, error) in
                guard error == nil else {
                    print(error?.localizedDescription)
                    return
                }
            })
        }
        
        let predicate: NSPredicate = NSPredicate(format: "name = %@", "Lily")
        let query: CKQuery = CKQuery(recordType: artworkRecord.recordType, predicate: predicate)
        publicDataBase.perform(query, inZoneWith: nil) { (records, error) in
            guard error == nil else {
                print(error?.localizedDescription)
                return
            }
            guard records!.count > 0 else {
                return
            }
            print(records![0])
        }
        let documentsURL = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first!
        let fileURL = (documentsURL).appending("/lily.jpg")
        
        let image = UIImage(named: "lily")!
        do {
            try UIImageJPEGRepresentation(image, 1.0)!.write(to: URL(fileURLWithPath: fileURL))
            let file: CKAsset = CKAsset(fileURL: URL(fileURLWithPath: fileURL))
            artworkRecord["image"] = file
            publicDataBase.save(artworkRecord, completionHandler: { (record, error) in
                guard error == nil else {
                    print(error?.localizedDescription)
                    return
                }
            })
        } catch let error {
            print(error.localizedDescription)
        }
        
        let prediacate = NSPredicate(format: "name = %@", "Lily")
        let query = CKQuery(recordType: "Lil", predicate: prediacate)
        publicDataBase.perform(query, inZoneWith: nil) { (record, error) in
            guard error == nil else {
                print(error?.localizedDescription)
                return
            }
            guard record!.count > 0 else {
                print("non records")
                return
            }
            let image = record![0]["image"] as! CKAsset
            let imageData = NSData(contentsOfFile: image.fileURL.path)
            DispatchQueue.main.async {
                self.imageView.image = UIImage(data: imageData as! Data)
            }
        }
        
    
        
       ckContainer.accountStatus { (accountStatus, error) in
        if( accountStatus == CKAccountStatus.noAccount) {
            let alert = UIAlertController(title: "Sign in to iCloud", message: "Sign in to your iCloud account to write records. On the Home screen, launch Settings, tap iCloud, and enter your Apple ID. Turn iCloud Drive on. If you don't have an iCloud account, tap Create a new Apple ID.", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Okay", style: .cancel, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
        
        }
 
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

